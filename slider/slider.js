let rndNum = Math.floor(Math.random() * 5)

const sliderPlugin = (activeSlide) => {
  const slides = document.querySelectorAll('.slide');

  slides[activeSlide].classList.add('active')

  for (const slide of slides) {
    slide.addEventListener('click', () => {
      if (slide.classList.contains('active')) {
        return slide.classList.remove('active');
      }
      clearActiveClasses()
      slide.classList.add('active')
    })
  }
  const clearActiveClasses = () => {
    slides.forEach((slide) => {
      slide.classList.remove('active');
    })
  }
}
sliderPlugin(rndNum)