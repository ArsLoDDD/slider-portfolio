const board = document.querySelector('#board');
const SQUARES_COUNTS = 460;


for (let I = 0; I < SQUARES_COUNTS; I++) {
  const square = document.createElement('div')
  square.classList.add('square')
  square.addEventListener('mouseover', () => {
    setColor(square)
  })
  square.addEventListener('mouseleave', () => {
    removeColor(square)
  })
  board.append(square)

}

function setColor(elem) {
  const frstColor = Math.floor(Math.random() * 9)
  const scdColor = Math.floor(Math.random() * 9)
  const trdColor = Math.floor(Math.random() * 9)

  const color = String(frstColor) + String(scdColor) + String(trdColor)
  elem.style.backgroundColor = `#${color}`;
  elem.style.boxShadow = `0 0 2px #${color}, 0 0 10px #${color}`;
}
function removeColor(elem) {
  elem.style.backgroundColor = '#1d1d1d'
  elem.style.boxShadow = `0 0 2px #000`;  
}