const startBtn = document.querySelector('#start')
const timeBtns = document.querySelector('#time-list')
const screens = document.querySelectorAll('.screen')
const timeEl = document.querySelector('#time')
const board = document.querySelector('#board')
const colorArray = ['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6',
  '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
  '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A',
  '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
  '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC',
  '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
  '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
  '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
  '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
  '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'];

let score = 0
let time = 0
let gameIsFinish = false
startBtn.addEventListener('click', (event) => {
  event.preventDefault()
  screens[0].classList.add('up')
})
timeBtns.addEventListener('click', (event) => {
  if (event.target.classList.contains('time-btn')) {
    time = parseInt(event.target.getAttribute('data-time'))
    screens[1].classList.add('up')
    startGame()
  }
})

board.addEventListener('click', (event) => {
  if (event.target.classList.contains('circle')) {
    score++
    event.target.remove()
    createRandomCircle()
  } else {
    if (gameIsFinish === false) {
      score--
    }
  }
})

function startGame() {
  createRandomCircle()

  setInterval(() => {
    decreaseTime()
  }, 1000);
  setCurrent(time)
}
function decreaseTime() {

  if (time === 0) {
    finishGame()
  } else {
    let currentTime = --time
    if (currentTime < 10) {
      currentTime = `0${currentTime}`
    }
    setCurrent(currentTime)
  }
}
function setCurrent(value) {
  if (value === 60) {
    return timeEl.innerHTML = `01:00`
  }
  return timeEl.innerHTML = `00:${value}`

}
function createRandomCircle() {
  const { width, height } = board.getBoundingClientRect()
  const circle = document.createElement('div')
  const size = getRndNum(5, 25)
  const x = getRndNum(0, width - size);
  const y = getRndNum(0, height - size);
  circle.classList.add('circle')
  circle.style.width = `${size}px`
  circle.style.height = `${size}px`
  circle.style.top = `${y}px`
  circle.style.left = `${x}px`
  circle.style.background = `${getRndColor()}`
  board.append(circle)
}
function getRndNum(min, max) {
  return Math.round(Math.random() * (max - min) + min)
}
function finishGame() {
  timeEl.parentNode.classList.add('hide')
  const finishScore = score
  board.innerHTML = `<h1>Счет: <span class="primary">${finishScore}</span></h1>`
  gameIsFinish = true
}
function getRndColor() {
  const colorIndex = Math.floor(Math.random() * (colorArray.length + 1))
  console.log(colorArray[colorIndex]);
  return colorArray[colorIndex]
}