
const upBtn = document.querySelector('.up-button');
const downBtn = document.querySelector('.down-button');
const sideBar = document.querySelector('.sidebar');
const mainSlide = document.querySelector('.main-slide');
const container = document.querySelector('.container')
const countSlides = mainSlide.querySelectorAll('div').length

let activeSlide = 0
sideBar.style.top = `-${(countSlides - 1) * 100}vh`


upBtn.addEventListener('click', () => {
  changeDirection('up')
})
downBtn.addEventListener('click', () => {
  changeDirection('down')
})

function changeDirection(direction) {
  if (direction === 'up') {
    activeSlide++
    console.log(activeSlide);
    if (activeSlide === countSlides) {
      activeSlide = 0;
    }
  } else if (direction === 'down') {
    activeSlide--
    if (activeSlide < 0) {
      activeSlide = countSlides - 1
    }
    console.log(activeSlide);

  }
  const height = container.clientHeight
  mainSlide.style.transform = `translateY(-${activeSlide * height}px)`;
  sideBar.style.transform = `translateY(${activeSlide * height}px)`;
}



